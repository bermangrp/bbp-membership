<?php
	ini_set('display_errors', 1);
	error_reporting(E_ALL);

	/**
	 * Template Name: bbPress - User Register
	 *
	 * @package bbPress
	 * @subpackage Theme
	 */

	//if user has POST data, register user and then automatically log them in

	if(
		(isset($_POST['username']) && isset($_POST['password']))
		&&
		($_POST['username'] != "" && $_POST['password'] != "")
	):
		
		$fields = array(
			'method' => 'login',
			'username' => $_POST['username'],
			'password' => $_POST['password']
		);

		$url = 'http://www.bomany.org/wp-content/themes/bomany/cvent/index.php?' . http_build_query($fields, '', "&");

        // create curl resource 
		$ch = curl_init(); 

        // set url 
		curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	    // $output contains the output string
		$output = curl_exec($ch); 

        // close curl resource to free up system resources
		curl_close($ch);  

		$is_valid_login = preg_replace( "/\r|\n/", "", $output);
		?>

		<?php if($is_valid_login == 'no'): ?>
			<?php 
				echo '<meta http-equiv="refresh" content="0;url=/members-only/?signup_error">';
			?>

		<?php else:
		$user_data = array(
			'user_email' => $_POST['username'],
			'user_login' => $_POST['username'],
			'user_pass' => $_POST['password']
		);

		if (!email_exists($user_data['user_email'])){
			$user_id = wp_insert_user($user_data);
			$new_role_forum_role = 'bbp_participant';

			bbp_set_user_role($user_id, $new_role_forum_role);
			user_login($user_id);
			redirect_to_home();
		} else {
			$user = get_user_by('email', $user_data['user_email']);
			$user_id = $user->ID;

			user_login($user_id);
			redirect_to_home();
		}
		?>
	<?php endif; ?>
<?php
	// else, print the page and show form	
	else:
		// No logged in users
		bbp_logged_in_redirect();

		// Begin Template
		get_header(); ?>

		<?php do_action( 'bbp_before_main_content' ); ?>

		<?php do_action( 'bbp_template_notices' ); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="container">
				<div class="row">
					<div class="signup_login_container">
						<div class="left_column">
							<div id="bbp-register" class="bbp-register">
								<div class="entry-content">
									<?php the_content(); ?>
									<div class="sign-up-box-container">
										<div id="bbpress-forums" class="sign-up-box">
											<h1>Member Sign-Up</h1>
											<?php echo display_form_boma(); 
												if(isset($_GET['signup_error'])){
													echo '<div class="sign_up_error">Invalid email address or password</div>';
												}
											?>
										</div>	
									</div>
								</div>
							</div>
						</div>
						<div class="right_column">
							<div class="login-box-container">
								<div class="login-box">
									<h1>Member Login</h1>
									<a href="/wp-login.php"><input type="button" value="Login"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

		<?php do_action( 'bbp_after_main_content' ); ?>
		<?php get_footer(); ?>

<?php endif; ?>