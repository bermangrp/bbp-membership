<?php
	/* Display BOMA form */
	function display_form_boma() {
		return "<form method='post'>
					<input type='text' name='username' placeholder='Email Address'><br>
					<input type='password' name='password' placeholder='Password'><br>
					<input type='submit' value='Submit'><br>
				</form>";
	}

	/* Load stylesheet into header */
	function my_custom_login() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.min.css" />';
	}
	add_action('login_head', 'my_custom_login');

	/* Change BOMA logo URL */
	function my_login_logo_url() {
		return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );

	/* Change BOMA logo title */
	function my_login_logo_url_title() {
		return 'BOMA Portal';
	}
	add_filter( 'login_headertitle', 'my_login_logo_url_title' );

	/* Remove the "Dashboard" from the admin menu for non-admin users */
	function wpse52752_remove_dashboard () {
		global $current_user, $menu, $submenu;
		get_currentuserinfo();

		if( ! in_array( 'administrator', $current_user->roles ) ) {
			reset( $menu );
			$page = key( $menu );
			while( ( __( 'Dashboard' ) != $menu[$page][0] ) && next( $menu ) ) {
				$page = key( $menu );
			}
			if( __( 'Dashboard' ) == $menu[$page][0] ) {
				unset( $menu[$page] );
			}
			reset($menu);
			$page = key($menu);
			while ( ! $current_user->has_cap( $menu[$page][1] ) && next( $menu ) ) {
				$page = key( $menu );
			}
			if ( preg_match( '#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI'] ) &&
				( 'index.php' != $menu[$page][2] ) ) {
				wp_redirect( get_option( 'siteurl' ) . '/wp-admin/edit.php');
			}	
		}
	}
	add_action('admin_menu', 'wpse52752_remove_dashboard');

	/* Remove WP update notice */
	function hide_update_notice() {
		get_currentuserinfo();
		if (!current_user_can('manage_options')) {
			remove_action( 'admin_notices', 'update_nag', 3 );
		}
	}
	add_action( 'admin_notices', 'hide_update_notice', 1 );

	/* Remove WP Admin Bar logo */
	function wpb_custom_logo() {
		echo '
			<style type="text/css">
				#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
					background-image: none !important;
					background-position: 0 0;
					color:rgba(0, 0, 0, 0);
				}
				#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
					background-position: 0 0;
				}
				#wp-admin-bar-wp-logo{
					display: none;	
				}
			</style>
		';
	}
	add_action('wp_before_admin_bar_render', 'wpb_custom_logo');

	/* Redirect Registration Page */
	function my_registration_page_redirect()
	{
		global $pagenow;

		if ( ( strtolower($pagenow) == 'wp-login.php') && ( strtolower( $_GET['action']) == 'register' ) ) {
			wp_redirect(home_url('/members-only'));
		}
	}
	add_filter( 'init', 'my_registration_page_redirect' );


	/* Remove "Private:" label from forum names */
	function ntwb_remove_private_title($title) {
		return '%s';
	}
	add_filter('private_title_format', 'ntwb_remove_private_title');

	/* Add Member Login/Logout button to menu */
	function add_last_nav_item($items, $args) {
		if(($args->menu == 'primary-header') && (is_user_logged_in())){
			return $items .= '<li><a href="/wp-login.php?action=logout" class="member_login_btn_container"><div class="member_login_btn">Log Out</div></a></li>';
		} else {
			return $items .= '<li><a href="/members-only" class="member_login_btn_container"><div class="member_login_btn">Members Only</div></a></li>';
		}
	}
	add_filter('wp_nav_menu_items','add_last_nav_item', 10, 2);

	/* Redirect to home page when you log in */
	function admin_default_page() {
		return '/';
	}
	add_filter('login_redirect', 'admin_default_page');

	/* Automatically log user in by setting auth cookies */
	function user_login($user_id) {
		wp_set_auth_cookie($user_id, true);
	}

	/* Generate BBporess Edit Profile Link in a shortcode */
	function bbp_edit_profile_link($atts) {
		extract(shortcode_atts(array(
			'text' => "",  // default value if none supplied
			'class' => "edit_profile_link" // Style class for link
	    ), $atts));

		$user_logged_in = is_user_logged_in();
	    
	    if($user_logged_in){
	    	if($text){
				$current_user = wp_get_current_user(); 
				$user = $current_user->display_name;
	        	return '<a class="'. $class . '" href="/forums/user/' . $user . '/edit">' . $text. '</a>';
	    	} 
	    }
	}
	// [bbp_edit_profile text="Edit My Profile" class"my-link-style"]
	
	// add_shortcode('bbp_edit_profile', 'bbp_edit_profile_link');
	

	/* Redirect to home page */
	function redirect_to_home(){
		echo 'Redirecting!';
		echo '<meta http-equiv="refresh" content="0;url=/">';
	}

	/* Redirect to home page when user logs out */
	function auto_redirect_after_logout(){
		wp_redirect( home_url() );
		exit();
	}
	add_action('wp_logout','auto_redirect_after_logout');

	/* Remove Admin Bar */
	show_admin_bar(false);
?>