## Create Droplet
----
- Go to [DigitalOcean](http://www.digitalocean.com)
- Go to One-click Apps
- Use Wordpress
- Use 1GB RAM (or more as needed)
- Add backups
- modify hostname to portal.bomany.org (or website name)


## Set Up Droplet
----

- SSH into server as root with password emailed to you

### Create new user
----

#	
	adduser [NEW_USERNAME]
	
- Modify the user's permissions

#
	sudo adduser [NEW_USERNAME] www-data
	sudo chown -R www-data:www-data /var/www/html
	sudo chmod -R g+w /var/www/html
	visudo
	
> 
	[NEW_USERNAME] ALL=(ALL:ALL) ALL
	
- Restart server and exit

#
	sudo service apache2 restart
	exit
	
### Set Up WordPress
----
- Go to IP address on browser

#### Finish WordPress installation
----
- Check off "Discourage search engines from indexing this site"

#### Set Up Theme
----
- Download repo from GitLab
- Upload repo to server
- Activate theme

#### Set Up Plugins
----
- Install "bbPress" and "Basic User Avatars"

#### Set Up Home Page
----
- Create page called Home
- Add the following shortcodes to your page

#
	[bbp_edit_profile text="Edit My Profile" class"my-link-style"]
	[bbp-forum-index]
	
- Change your reading settings to read from this new home page	
	
#### Set Up Members Only Page
----
- Create page called Members Only
- Change template to "bbPress - User Register"

#### Set Up Menu Bar
----
- Create new menu with same links as [bomany.org](http://www.bomany.org/)