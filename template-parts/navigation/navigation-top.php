<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="#"></a>
	<?php 
	$defaults = array(
		'theme_location'  => 'primary-header',
		'menu'            => 'primary-header',
		'container'				=> 'div',
		'container_class' => 'collapse navbar-collapse',
		'container_id'		=> 'navbarNav',		      
		'menu_class'      => 'navbar-nav',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '<span>',
		'link_after'      => '</span>',		      
		'depth'           => 0,
		'walker'          => ''
	);
	wp_nav_menu( $defaults );
	?>
</nav>
