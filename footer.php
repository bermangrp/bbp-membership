<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer class="footer">
                <div class="ad_block displayNone">
                    <div class="container">
                        <h6>BOMA NEW YORK ADVERTISERS</h6>
                        <div class="ad_section"></div>
                    </div>
                </div>
                <div class="footer_info_block">
                    <div class="container">
                        <div class="social_logo_container">
                            <div class="social_media_block">
                                <ul>
                                    <li>
                                       <a href="https://www.facebook.com/pg/BOMANewYork/about" target="_blank" class="hyperlink"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                   </li>
                                   <li>
                                       <a href="https://twitter.com/boma_ny?lang=en" target="_blank" class="hyperlink"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                   </li>
                                   <li>
                                       <a href="https://www.linkedin.com/company/boma-new-york" target="_blank" class="hyperlink"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                   </li>
                                   <li>
                                       <a href="https://www.instagram.com/bomanewyork/" target="_blank" class="hyperlink"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                   </li>
                                </ul>
                            </div>
                            <div class="footer_logo_block">
                                <div class="footer_logo">
                                    <div class="displayTable height100percent">
                                        <div class="displayTableCell">
                                            <p>Federated with</p>
                                            <a href="http://www.boma.org/" class="hyperlink" target="_blank"><img src="http://www.bomany.org/wp-content/themes/bomany-la/images/graphics/boma_intl_logo.jpg" alt="BOMA NY – Building Owners &amp; Managers Association of Greater New York, Inc."></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer_nav_block">
                            <div class="footer_nav_block_container">
                                <div class="displayTable height100percent">
                                    <div class="displayTableCell">
                                        <div id="footer_navbar" class="footer_container">
                                            <?php 
                                                $defaults = array(
                                                  'theme_location'  => 'primary-header',
                                                  'menu'            => 'primary-header',
                                                  'container'       => 'div',
                                                  'container_class' => 'footer_container',
                                                  'container_id'    => 'footer_navbar',           
                                                  'menu_class'      => 'navbar-nav',
                                                  'echo'            => true,
                                                  'fallback_cb'     => 'wp_page_menu',
                                                  'before'          => '',
                                                  'after'           => '',
                                                  'link_before'     => '<span>',
                                                  'link_after'      => '</span>',             
                                                  'depth'           => 0,
                                                  'walker'          => ''
                                                );
                                                wp_nav_menu( $defaults );
                                            ?>
                                        </div>          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer_contact_block">
                            <div class="footer_address">
                                <p>One Penn Plaza | Suite 2205 | New York, NY 10119 | <span><a href="tel:+12122393662" class="hyperlink">212.239.3662</a></span></p>
                            </div>
                            <div class="footer_copyright">
                                <p>
                                    Copyright © 2018 BOMA | NY. All rights reserved | <span>Website by <a href="http://www.bermangrp.com/" class="hyperlink">The Berman Group</a></span>
                                </p>    
                            </div>
                        </div>                        
                    </div>
                </div>
            </footer>
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
